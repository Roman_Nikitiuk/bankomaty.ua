//
//  RESTApiManager.swift
//  Bankomaty.UA
//
//  Created by Roman Nikitiuk on 9/21/17.
//  Copyright © 2017 RN. All rights reserved.
//

import Foundation
import Alamofire

enum BankomatyURLs:String{
    case serverURL = "https://api.privatbank.ua/p24api/"
    case getATMsList = "infrastructure?json&atm&address=&city="
}

class RESTApiManager{
    static let serverURL = BankomatyURLs.serverURL.rawValue

    class func isConnectedToInternet() ->Bool {
        noInternetView.removeFromSuperview()
        return NetworkReachabilityManager()!.isReachable
    }

    class func getData(_ target:     UIViewController?,
                       _ url:        BankomatyURLs,
                       _ parameters: String?,
                       _ completion: @escaping(AnyObject) -> () ){
       
        target?.present(preloader(), animated: true, completion: nil)
     
        var urlRequest = serverURL + url.rawValue + (parameters ?? "")
        urlRequest = urlRequest.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print("...\n.Get data from: \(url), \n..\(urlRequest)")
        
        Alamofire.request(urlRequest).responseJSON { response in
            if target?.presentedViewController == nil {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    target?.dismiss(animated: true, completion: nil)
                }
            }
            target?.dismiss(animated: true, completion: nil)
            print("... response receive from \(url)")
            print(".... ",response.result.value as Any)
            
            switch response.result{
            case .success:
                completion(response.result.value as AnyObject)
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }

    
    //  MARK: -- Preloader
    class func preloader() -> UIAlertController{
        let preloader = UIAlertController(title: " ", message: "Loading... Please Wait", preferredStyle: .alert)
        let indicator = UIActivityIndicatorView()
        indicator.center = CGPoint(x:135.0, y:22)
        indicator.color = UIColor.black
        preloader.view.addSubview(indicator)
        indicator.startAnimating()
        return preloader
    }
    
    // MARK: -- NO Internet View
    static var noInternetView = NoInternetView()
    class func showNoInternetView(_ target: UIViewController?,_ action: Selector){
        noInternetView.removeFromSuperview()
        if let target = target {
            UIView.animate(withDuration: 0.5, animations: {
                noInternetView = NoInternetView(frame: target.view.frame)
                noInternetView.addGestureRecognizer(UITapGestureRecognizer(target: target, action: action ))
                target.view.window?.addSubview(noInternetView)
            })
        }
    }

}


struct ATMsData {
    var type:           String!
    var cityUA:         String!
    var fullAddressUa:  String!
    var placeUa:        String!
    var latitude:       String!
    var longitude:      String!
    var hours:          String!
    
    init(_ atmData: [String:AnyObject]) {
        type = atmData["type"] as? String ?? ""
        cityUA = atmData["cityUA"] as? String ?? ""
        fullAddressUa = atmData["fullAddressUa"] as? String ?? ""
        placeUa = atmData["placeUa"] as? String ?? ""
        latitude = atmData["latitude"] as? String ?? ""
        longitude = atmData["longitude"] as? String ?? ""
        if let tw = atmData["tw"] as? [String:String]{
            let today = Calendar(identifier: .gregorian).component(.weekday, from: Date())
            switch today {
            case 1:
                hours = "Відкрито: \(tw["sun"] ?? "")"
            case 2:
                hours = "Відкрито: \(tw["mon"] ?? "")"
            case 3:
                hours = "Відкрито: \(tw["tue"] ?? "")"
            case 4:
                hours = "Відкрито: \(tw["wed"] ?? "")"
            case 5:
                hours = "Відкрито: \(tw["thu"] ?? "")"
            case 6:
                hours = "Відкрито: \(tw["fri"] ?? "")"
            case 7:
                hours = "Відкрито: \(tw["sat"] ?? "")"
            default:
                hours = "Відкрито: \(tw["hol"] ?? "")"
            }
        } else {
            hours = ""
        }
        
    }
}













