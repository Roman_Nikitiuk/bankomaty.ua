//
//  NoInternetView.swift
//  LeMall
//
//  Created by Roman Nikitiuk on 6/13/17.
//  Copyright © 2017 Roman Nikitiuk. All rights reserved.
//

import UIKit

class NoInternetView: UIView {
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backButtonAction(_ sender: Any) {
        self.removeFromSuperview()
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "goBack"), object: nil)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialization()
    }
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        initialization()
    }
    
    func initialization(){
        let view = Bundle.main.loadNibNamed("NoInternetView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }

}
