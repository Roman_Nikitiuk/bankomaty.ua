//
//  ViewController.swift
//  Bankomaty.UA
//
//  Created by Roman Nikitiuk on 9/21/17.
//  Copyright © 2017 RN. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController,UISearchBarDelegate,CLLocationManagerDelegate,GMSMapViewDelegate {

    let searchBar = UISearchBar()
   
    var location:         CLLocation!
    let locationManager = CLLocationManager()
    
    var atmsList = [ATMsData]()
    var cities:    [String]!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet var dropDownView: UIView!
    @IBOutlet weak var citiesCollectionView: UICollectionView!
    
    
    
    // MARK: -- LOAD VIEW --
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.placeholder = "Введите название города"
        
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = dropDownButton()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        mapView.accessibilityElementsHidden = false
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        
        cities = ["Киев","Одесса","Харьков",
                  "Львов","Запорожье","Днепропетровск",
                  "Чернигов","Луцк","Винница",
                  "Херсон","Ровно","Житомир",
                  "Ужгород","Сумы","Хмельницкий"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    // MARK: -- SET MAP --
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if location == nil {
            location = locations[0]
            mapView.camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15)
        }
        location = locations[0]
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        searchBar.endEditing(true)
    }

    
    
    
    // MARK: -- DROP DOWN LIST --
    
    func dropDownButton() -> UIBarButtonItem{
        let h = self.navigationController?.navigationBar.frame.height ?? 44
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: h, height: h))
        button.setImage(UIImage(named: "drop_down_list"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: -5, right: 0)
        button.addTarget(self, action: #selector(dropDownList), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    func dropDownList(){
        dropDownView.tag = dropDownView.tag == 0 ? openList() : hidelist()
    }
    func openList() -> Int {
        dropDownView.frame = CGRect(x: 0, y: -126, width: self.view.frame.width, height: 126)
        self.view.addSubview(dropDownView)
        UIView.animate(withDuration: 0.5) {
            self.dropDownView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.dropDownView.frame.height)
            self.citiesCollectionView.showsVerticalScrollIndicator = true
        }
        return 1
    }
    func hidelist() -> Int{
        UIView.animate(withDuration: 0.5, animations: {
            self.dropDownView.frame = CGRect(x: 0, y: -126, width: self.view.frame.width, height: self.dropDownView.frame.height)
        }) { (true) in
            self.dropDownView.removeFromSuperview()
        }
        return 0
    }
    
    
    
    // MARK: -- SHOW ATMs --
    
    func showATMs(){
        var dis: CLLocationDistance!
        var loc = CLLocation()
        
        mapView.clear()
        for atm in atmsList {
            addATMMarkers(atm)
        
        // find the nearest atm
            let loc2 = CLLocation(latitude: Double(atm.latitude) ?? 0.0, longitude: Double(atm.longitude) ?? 0.0)
            let dis2 = self.location.distance(from: loc2)
            if dis == nil {
                dis = dis2
                loc = loc2
            } else if dis2 < dis {
                dis = dis2
                loc = loc2
            }
        }
        
        let bounds = GMSCoordinateBounds(coordinate: self.location.coordinate, coordinate: loc.coordinate)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 40)
        mapView.animate(with: update)
    }
    
    func addATMMarkers(_ atm: ATMsData){
        let coordinate = CLLocationCoordinate2D(latitude: Double(atm.latitude) ?? 0.0, longitude: Double(atm.longitude) ?? 0.0)
        let marker = GMSMarker(position: coordinate)
        marker.title = atm.placeUa
        marker.snippet = "\(atm.fullAddressUa!)\n\(atm.hours!)"
        marker.icon = UIImage(named:"atm_icon")
        marker.map = self.mapView
    }
    

    
    // MARK: -- SEARCH FOR ATMs --
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        dropDownView.tag = dropDownView.tag == 1 ? hidelist() : 0
        getData()
    }
    func getData(){
        if RESTApiManager.isConnectedToInternet() {
            searchCity(searchBar.text)
        } else {
            RESTApiManager.showNoInternetView(self, #selector(getData))
        }
    }
    func searchCity(_ city: String?){
        RESTApiManager.getData(self, .getATMsList, city) { (response) in
            if let devices = response["devices"] as? [[String:AnyObject]]{
                if devices.count == 0 {
                    Config.showAlert(self, "Помилкова адреса!", "Не знайдено жодного банкомату.", .close){}
                } else {
                    self.atmsList.removeAll()
                    for dev in devices {
                        let atm = ATMsData(dev)
                        self.atmsList.append(atm)
                    }
                    self.showATMs()
                }
            }
        }
    }
    
    
}



// MARK: -- COLLECTION VIEW --

extension ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cities.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (self.view.frame.width - 1)/3
        return CGSize(width: w, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CitiesCell", for: indexPath) as! CitiesCell
        cell.title.text = cities[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.text = cities[indexPath.row]
        dropDownList()
        searchBarSearchButtonClicked(searchBar)
    }
    
}

class CitiesCell: UICollectionViewCell{
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        title.text = ""
    }
}














