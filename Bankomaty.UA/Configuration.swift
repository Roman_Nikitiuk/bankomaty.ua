//
//  Configuration.swift
//  Bankomaty.UA
//
//  Created by Roman Nikitiuk on 9/24/17.
//  Copyright © 2017 RN. All rights reserved.
//

import Foundation
import UIKit

class Config {
    
    class func showAlert(_ target: UIViewController?,_ title: String?,_ message: String?,_ buttonTitle: AlerButtonTitle, completion: @escaping () -> Void){
        //target.dismiss(animated: false, completion: nil)
        let actionSheetController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: buttonTitle.rawValue, style: .default) { action -> Void in ( completion() )}
        actionSheetController.addAction(closeAction)
        target?.present(actionSheetController, animated: true, completion: nil)
    }
    
}

enum AlerButtonTitle: String{
    case close = "Close"
    case ok = "OK"
}
